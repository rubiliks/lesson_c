#include <iostream>


class Animal
{	
public:
	virtual void voice() const = 0 
	{
		std::cout << "Voice!" << std::endl;
	};

};

class Dog : public Animal
{
public:
	void voice()  const override
	{
		std::cout << "Wow!" << std::endl;
	}
};

class Cat : public Animal 
{
public:
	void voice()  const override
	{
		std::cout << "Mey!" <<std::endl;
	}
};


class Cow : public Animal 
{
public:
	void voice()  const override
	{
		std::cout << "My!" << std::endl;
	}
};

int main()
{
	Animal* testAniml[3] ;

	testAniml[0] = new Dog();
	testAniml[1] = new Cat();
	testAniml[2] = new Cow();


	for (auto a : testAniml)
	{
		a->voice();

	}

};